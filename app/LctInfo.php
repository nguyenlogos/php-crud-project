<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LctInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lct_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['lctno', 'lct_cd', 'lct_nm', 'up_lct_cd', 'level_cd', 'index_cd', 'shelf_cd', 'order_no', 'real_use_yn', 're_mark', 'use_yn', 'lct_rfid', 'lct_bar_cd', 'mt_yn', 'mv_yn', 'rt_yn', 'manager_id', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 'mn_full', 'sap_lct_cd', 'userid', 'selected'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
