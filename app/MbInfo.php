<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mb_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['userid', 'uname', 'nick_name', 'upw', 'grade', 'tel_nb', 'cel_nb', 'e_mail', 'sms_yn', 'scr_yn', 'mail_yn', 'join_ip', 'join_domain', 'ltacc_dt', 'ltacc_domain', 'mbout_dt', 'mbout_yn', 'accblock_yn', 'session_key', 'session_limit', 'memo', 'del_yn', 'check_yn', 'rem_me', 'mbjoin_dt', 'log_ip', 'lct_cd', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 're_mark'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['ltacc_dt', 'mbout_dt', 'session_limit', 'mbjoin_dt', 'reg_dt', 'chg_dt'];

}
