<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommDt extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comm_dt';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cdid', 'mt_cd', 'dt_cd', 'dt_nm', 'dt_kr', 'dt_vn', 'dt_exp', 'up_cd', 'val1', 'val1_nm', 'val2', 'val2_nm', 'val3', 'val3_nm', 'val4', 'val4_nm', 'dt_order', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 'unit'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
