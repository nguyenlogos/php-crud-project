<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'department_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['dpno', 'depart_cd', 'depart_nm', 'up_depart_cd', 'level_cd', 're_mark', 'use_yn', 'order_no', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 'mn_full'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
