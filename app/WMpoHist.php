<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WMpoHist extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'w_mpo_hist';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['wmid', 'wm_no', 'mdpo_no', 'mt_no', 'md_sts_cd', 'lct_cd', 'lct_sts_cd', 'from_lct_cd', 'to_lct_cd', 'output_dt', 'input_dt', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
