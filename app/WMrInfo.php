<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WMrInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'w_mr_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mrid', 'mr_no', 'mr_sts_cd', 'lct_cd', 'worker_id', 'manager_id', 'req_rec_dt', 'real_rec_dt', 'mt_qty', 'rel_bom', 'remark', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
