<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMpoDetal extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'p_mpo_detal';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pdid', 'mpo_no', 'mdpo_no', 'mt_no', 'md_sts_cd', 'lct_cd', 'exp_input_dt', 'need_qty', 'feed_size', 'feed_unit', 'act_pur_qty', 'unit_price', 'price_unit_cd', 'tot_pur_amt', 'last_unit_amt', 'tot_last_amt', 'vat_cd', 'vat_amt', 'pay_amt', 'act_pay_amt', 'rec_input_dt', 'pass_qty', 'bundle_unit_qty', 'bundle_qty', 'rec_qty', 'defect_qty', 'def_reson_cd', 'return_qty', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 'lct_sts_cd'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
