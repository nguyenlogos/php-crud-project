<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'equipment_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['emno', 'as_no', 'as_cd', 'as_nm', 'srl_no', 'mac_add', 'sts_cd', 'lct_cd', 'lct_sts_cd', 'mgm_dept_cd', 'cost_dept_cd', 'unit', 'quantity', 'mf_cd', 'md_cd', 'sp_cd', 'eq_prc', 'eq_prc_unit', 'barcode', 'dev_start_dt', 'puchs_dt', 'vailed_dt', 're_mark', 'from_lct_cd', 'to_lct_cd', 'div_cd', 'charge_id', 'borrow_id', 'return_id', 'mgm_id', 'receive_id', 'output_dt', 'input_dt', 'disps_dt', 'attach_cd', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
