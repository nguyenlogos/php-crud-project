<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DMaterialInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'd_material_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mtid', 'mt_type', 'mt_no', 'mt_nm', 'mf_cd', 'sp_cd', 'width', 'width_unit', 'spec', 'spec_unit', 'price', 'price_unit', 'photo_file', 're_mark', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
