<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DProcessPartInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'd_process_part_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ppid', 'process_no', 'part_no', 'part_nm', 'man_num', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 'photo_file'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
