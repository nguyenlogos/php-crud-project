<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'model_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mdno', 'mf_cd', 'md_cd', 'md_nm', 'md_vn_nm', 'prt_cd', 'tp_cd', 'group_cd', 'func_cd', 'photo_file', 'dimension', 'elect', 'elect_unit_cd', 'e_cst', 'ecst_unit_cd', 'wgt', 'wgt_unit_cd', 're_mark', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
