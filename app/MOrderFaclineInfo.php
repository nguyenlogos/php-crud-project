<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MOrderFaclineInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_order_facline_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['oflno', 'ofl_no', 'fo_no', 'qty_day_line', 'line_qty', 'day_qty', 'need_day', 'total_need_day', 'fo_qty', 'po_no', 'lct_cd', 'product_dt', 'product_real_dt', 're_mark', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
