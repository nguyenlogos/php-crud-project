<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SOrderInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 's_order_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['soid', 'po_no', 'bom_no', 'order_dt', 'product_dt', 'delivery_dt', 'dest_cd', 'delivery_qty', 'fo_qty', 'so_qty', 'fo_rm_qty', 'so_rm_qty', 're_mark', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
