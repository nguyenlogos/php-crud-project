<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mnno', 'mn_cd', 'mn_nm', 'up_mn_cd', 'level_cd', 'url_link', 're_mark', 'col_css', 'sub_yn', 'order_no', 'use_yn', 'mn_full', 'mn_cd_full', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 'selected'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
