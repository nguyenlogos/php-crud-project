<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebpagesMembership extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'webpages_membership';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['UserId', 'CreateDate', 'ConfirmationToken', 'IsConfirmed', 'LastPasswordFailureDate', 'PasswordFailuresSinceLastSuccess', 'Password', 'PasswordChangedDate', 'PasswordSalt', 'PasswordVerificationToken', 'PasswordVerificationTokenExpirationDate'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['CreateDate', 'LastPasswordFailureDate', 'PasswordChangedDate', 'PasswordVerificationTokenExpirationDate'];

}
