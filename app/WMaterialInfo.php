<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WMaterialInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'w_material_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['wmtid', 'pdid', 'mt_cd', 'mt_no', 'mpo_no', 'mdpo_no', 'mt_barcode', 'mt_sts_cd', 'lct_cd', 'lct_sts_cd', 'from_lct_cd', 'to_lct_cd', 'output_dt', 'input_dt', 'worker_id', 'sd_no', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
