<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MOrderFaclineDay extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'm_order_facline_day';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['olddno', 'oldd_no', 'old_no', 'line_cd', 'work_dt', 'prod_qty', 'done_qty', 'move_qty', 'refer_qty', 're_mark', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
