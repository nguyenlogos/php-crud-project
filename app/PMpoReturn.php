<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PMpoReturn extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'p_mpo_return';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mpid', 'rt_no', 'pdid', 'mpo_no', 'mdpo_no', 'mt_no', 'md_sts_cd', 'lct_cd', 'defect_qty', 'def_reson_cd', 'return_qty', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt', 'lct_sts_cd'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
