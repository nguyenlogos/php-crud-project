<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierInfo extends Model  
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supplier_info';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['spno', 'sp_cd', 'sp_nm', 'bsn_tp', 'changer_id', 'phone_nb', 'cell_nb', 'fax_nb', 'e_mail', 'web_site', 'address', 're_mark', 'use_yn', 'del_yn', 'reg_id', 'reg_dt', 'chg_id', 'chg_dt'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['reg_dt', 'chg_dt'];

}
